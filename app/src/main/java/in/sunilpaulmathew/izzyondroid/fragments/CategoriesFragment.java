package in.sunilpaulmathew.izzyondroid.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;
import in.sunilpaulmathew.izzyondroid.utils.tasks.CategoriesLoadingTask;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class CategoriesFragment extends Fragment {

    private AppCompatEditText mSearchWord;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_categories, container, false);

        AppCompatImageButton mBack = mRootView.findViewById(R.id.back);
        AppCompatImageButton mSearch = mRootView.findViewById(R.id.search);
        mSearchWord = mRootView.findViewById(R.id.search_word);
        MaterialTextView mTitle = mRootView.findViewById(R.id.title);
        ProgressBar mProgress = mRootView.findViewById(R.id.progress);
        RecyclerView mRecyclerView = mRootView.findViewById(R.id.recycler_view);

        mTitle.setText(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle());

        mRecyclerView.setLayoutManager(new GridLayoutManager(requireActivity(), RecyclerViewData.getSpanCount(6, 3, requireActivity())));
        new CategoriesLoadingTask(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle(), mProgress, mRecyclerView, requireActivity()).execute();

        mSearch.setOnClickListener(v -> {
            if (mSearchWord.getVisibility() == View.VISIBLE) {
                mSearchWord.setVisibility(View.GONE);
                mTitle.setVisibility(View.VISIBLE);
                Common.toggleKeyboard(mSearchWord, 0, requireActivity());
            } else {
                mSearchWord.setVisibility(View.VISIBLE);
                mTitle.setVisibility(View.GONE);
                Common.toggleKeyboard(mSearchWord, 1, requireActivity());
            }
        });

        mSearchWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Common.setSearchText(s.toString().toLowerCase());
                new CategoriesLoadingTask(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle(), mProgress, mRecyclerView, requireActivity()).execute();
            }
        });

        mBack.setOnClickListener( v-> {
            Common.setSearchText(null);
            requireActivity().finish();
        });

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (Common.getSearchText() != null && !Common.getSearchText().isEmpty()) {
                    Common.setSearchText(null);
                    mSearchWord.setText(null);
                    new CategoriesLoadingTask(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle(), mProgress, mRecyclerView, requireActivity()).execute();
                } if (mSearchWord.getVisibility() == View.VISIBLE) {
                    mSearchWord.setVisibility(View.GONE);
                    mTitle.setVisibility(View.VISIBLE);
                } else {
                    requireActivity().finish();
                }
            }
        });

        return mRootView;
    }

}