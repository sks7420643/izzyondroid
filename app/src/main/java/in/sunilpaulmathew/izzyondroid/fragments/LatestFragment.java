package in.sunilpaulmathew.izzyondroid.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.activities.PackageSearchingActivity;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;
import in.sunilpaulmathew.izzyondroid.utils.tasks.LatestAppsLoadingTask;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class LatestFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_latest, container, false);

        FloatingActionButton mFAB = mRootView.findViewById(R.id.fab);
        ProgressBar mProgress = mRootView.findViewById(R.id.progress);
        RecyclerView mRecyclerView = mRootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(requireActivity(), RecyclerViewData.getSpanCount(4, 2, requireActivity())));
        new LatestAppsLoadingTask(mProgress, mRecyclerView, requireActivity()).execute();

        mFAB.setOnClickListener(v -> {
            Intent searching = new Intent(requireActivity(), PackageSearchingActivity.class);
            startActivity(searching);
        });

        return mRootView;
    }

}