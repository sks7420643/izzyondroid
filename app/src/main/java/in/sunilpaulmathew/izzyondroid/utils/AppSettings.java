package in.sunilpaulmathew.izzyondroid.utils;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.android.material.textview.MaterialTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import in.sunilpaulmathew.izzyondroid.MainActivity;
import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.receivers.UpdateReceiver;
import in.sunilpaulmathew.sCommon.APKUtils.sAPKUtils;
import in.sunilpaulmathew.sCommon.CommonUtils.sCommonUtils;
import in.sunilpaulmathew.sCommon.Dialog.sSingleChoiceDialog;
import in.sunilpaulmathew.sCommon.FileUtils.sFileUtils;
import in.sunilpaulmathew.sCommon.PackageUtils.sPackageUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on November 11, 2021
 */
public class AppSettings {

    private static int getUpdateIntervalPosition(Context context) {
        int interval = sCommonUtils.getInt("updateInterval", 24, context);
        for (int i = 0; i < getUpdateIntervalOptions(context).length; i++) {
            if (Integer.parseInt(getUpdateIntervalOptions(context)[i].replace(" hour","")) == interval) {
                return i;
            }
        }
        return 0;
    }

    private static int getTimeFramePosition(Context context) {
        long timeFrame = sCommonUtils.getLong("latestTimeFrame", 240, context);
        for (int i = 0; i < getTimeFrameOptions(context).length; i++) {
            if (Integer.parseInt(getTimeFrameOptions(context)[i].replace(" hour","")) == timeFrame) {
                return i;
            }
        }
        return 0;
    }

    public static sSingleChoiceDialog setUpdateInterval(MaterialTextView summary, Context context) {
        return new sSingleChoiceDialog(R.drawable.ic_update, context.getString(R.string.update_check_interval), getUpdateIntervalOptions(context),
                getUpdateIntervalPosition(context), context) {
            @Override
            public void onItemSelected(int position) {
                switch (position) {
                    case 0:
                        sCommonUtils.saveInt("updateInterval", 0, context);
                        configureUpdateCheck(context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "0"));
                        break;
                    case 1:
                        sCommonUtils.saveInt("updateInterval", 1, context);
                        configureUpdateCheck(context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "1"));
                        break;
                    case 2:
                        sCommonUtils.saveInt("updateInterval", 3, context);
                        configureUpdateCheck(context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "3"));
                        break;
                    case 3:
                        sCommonUtils.saveInt("updateInterval", 6, context);
                        configureUpdateCheck(context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "6"));
                        break;
                    case 4:
                        sCommonUtils.saveInt("updateInterval", 12, context);
                        configureUpdateCheck(context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "12"));
                        break;
                    case 5:
                        sCommonUtils.saveInt("updateInterval", 24, context);
                        configureUpdateCheck(context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "24"));
                        break;
                    case 6:
                        sCommonUtils.saveInt("updateInterval", 48, context);
                        configureUpdateCheck(context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "48"));
                        break;
                }
            }
        };
    }

    public static sSingleChoiceDialog setTimeFrame(MaterialTextView summary, Context context) {
        return new sSingleChoiceDialog(R.drawable.ic_update, context.getString(R.string.latest_timeframe), getTimeFrameOptions(context),
                getTimeFramePosition(context), context) {
            @Override
            public void onItemSelected(int position) {
                switch (position) {
                    case 0:
                        sCommonUtils.saveLong("latestTimeFrame", 24, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "24"));
                        break;
                    case 1:
                        sCommonUtils.saveLong("latestTimeFrame", 48, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "48"));
                        break;
                    case 2:
                        sCommonUtils.saveLong("latestTimeFrame", 72, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "72"));
                        break;
                    case 3:
                        sCommonUtils.saveLong("latestTimeFrame", 96, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "96"));
                        break;
                    case 4:
                        sCommonUtils.saveLong("latestTimeFrame", 120, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "120"));
                        break;
                    case 5:
                        sCommonUtils.saveLong("latestTimeFrame", 240, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "240"));
                        break;
                }
            }
        };
    }

    private static String[] getUpdateIntervalOptions(Context context) {
        return new String[] {
                context.getString(R.string.update_check_interval_summary, "0"),
                context.getString(R.string.update_check_interval_summary, "1"),
                context.getString(R.string.update_check_interval_summary, "3"),
                context.getString(R.string.update_check_interval_summary, "6"),
                context.getString(R.string.update_check_interval_summary, "12"),
                context.getString(R.string.update_check_interval_summary, "24"),
                context.getString(R.string.update_check_interval_summary, "48")
        };
    }

    private static String[] getTimeFrameOptions(Context context) {
        return new String[] {
                context.getString(R.string.update_check_interval_summary, "24"),
                context.getString(R.string.update_check_interval_summary, "48"),
                context.getString(R.string.update_check_interval_summary, "72"),
                context.getString(R.string.update_check_interval_summary, "96"),
                context.getString(R.string.update_check_interval_summary, "120"),
                context.getString(R.string.update_check_interval_summary, "240")
        };
    }

    public static void configureUpdateCheck(Context context) {
        AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent mIntent = new Intent(context, UpdateReceiver.class);
        @SuppressLint("UnspecifiedImmutableFlag")
        PendingIntent mPendingIntent = PendingIntent.getBroadcast(context, 0, mIntent, PendingIntent.FLAG_IMMUTABLE);
        sCommonUtils.saveLong("ucTimeStamp", System.currentTimeMillis(), context);
        mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + Common.getInterval(context) + 60000, mPendingIntent);
    }

    public static void showUpdateNotification(Context context) {
        List<String> mAppsList = new ArrayList<>();
        for (RecyclerViewItems item : RecyclerViewData.getInstalled(context)) {
            if (!sFileUtils.exist(new File(context.getExternalFilesDir("ignoreList"), item.getPackageName())) &&
                    Integer.parseInt(item.getVersionCode()) > sAPKUtils.getVersionCode(
                    sPackageUtils.getSourceDir(item.getPackageName(), context), context)) {
                mAppsList.add(item.getTitle());
            }
        }
        if (mAppsList.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String apps : mAppsList) {
                sb.append("- ").append(apps).append("\n");
            }
            Uri mAlarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Intent mIntent = new Intent(context, MainActivity.class);
            mIntent.putExtra(Common.getUpdateStatus(), "UPDATE_AVAILABLE");
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent mPendingIntent = PendingIntent.getActivity(context, 0, mIntent, PendingIntent.FLAG_IMMUTABLE);
            NotificationChannel mNotificationChannel = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                mNotificationChannel = new NotificationChannel("channel", context.getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
            }
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "channel");
            Notification mNotification = mBuilder.setContentTitle(context.getString(R.string.update_available))
                    .setContentText(context.getString(R.string.update_available_message, sb))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setSmallIcon(R.drawable.ic_update)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setContentIntent(mPendingIntent)
                    .setOnlyAlertOnce(true)
                    .setSound(mAlarmSound)
                    .setAutoCancel(true)
                    .build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager.createNotificationChannel(mNotificationChannel);
            }
            try {
                notificationManager.notify(0, mNotification);
            } catch (NullPointerException ignored) {}
        }
    }

}