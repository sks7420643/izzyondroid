* Migrated to latest Shizuku APi
* App now uses Shizuku API for uninstallation if available.
* Depreciated Android 6 support.
* AUpdated build tools.
* Miscellaneous changes.